# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2019-10-07

### New features

- Test Suite type definitions are now also a top-level export. Thanks @fobdy for the MR: https://gitlab.com/Vinnl/junit-xml/merge_requests/2

## [1.1.0] - 2019-09-26

### New features

- [New option](https://www.npmjs.com/package/junit-xml#schema-default-ant-junit) to have the output conform to the Azure DevOps expected JUnit format.

## [1.0.0] - 2019-01-18

### New features

- First release!
